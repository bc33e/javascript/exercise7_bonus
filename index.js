// BÀI 1
function btnInBangSo() {
  var ketQua = "";

  for (var i = 1; i < 100; i += 10) {
    for (var j = i; j < i + 10; j++) {
      ketQua += j + " ";
    }

    ketQua += "<br/>";
  }
  document.querySelector("#ket-qua-1").innerHTML = `${ketQua}`;
}

// BÀI 2
function kiemTraSoNguyenTo(number) {
  var kiemTra = 1;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      kiemTra = 0;
      break;
    }
  }
  return kiemTra;
}

var arrNum = [];
function btnNhapSo() {
  if (document.querySelector("#txt-day-so").value.trim() == "") {
    return;
  }
  var nhapSo = document.querySelector("#txt-day-so").value * 1;
  arrNum.push(nhapSo);
  console.log("arrNum: ", arrNum);
  document.querySelector("#txt-day-so").value = "";
}

function btnInSoNguyenTo() {
  var ketQua = "";
  for (var i = 1; i < arrNum.length; i++) {
    var kiemTra = kiemTraSoNguyenTo(arrNum[i]);
    if (kiemTra) {
      ketQua += arrNum[i] + " ";
    }
  }

  document.querySelector("#ket-qua-2").innerHTML = `<div>
        <p> Số nguyên tố: ${ketQua}</p>
        </div>`;
}

// BÀI 3
function tinhTong(number) {
  var tongS = 0;
  for (var i = 2; i <= number; i++) {
    tongS += i;
  }
  tongS = tongS + 2 * (i - 1);
  return tongS;
}
function btnTinhTong() {
  var soN = document.querySelector("#txt-so-n").value * 1;
  console.log("soN: ", soN);
  var ketQua = tinhTong(soN);

  document.querySelector("#ket-qua-3").innerHTML = `<div>
        <p> Kết quả: ${ketQua}</p>
        </div>`;
}

// BÀI 4
function btnSoLuongUocSo() {
  var soM = document.querySelector("#txt-so-m").value * 1;
  console.log("soM: ", soM);
  var soLuong = "";
  for (var i = 1; i <= soM; i++) {
    if (soM % i == 0) {
      soLuong += i + " ";
    }
  }
  document.querySelector("#ket-qua-4").innerHTML = `<div>
        <p> Ước số: ${soLuong}</p>
        </div>`;
}

// BÀI 5
function daoNguocSo(number) {
  number = number.toString();
  return number.split("").reverse().join("");
}

function btnSoDaoNguoc() {
  var soX = document.querySelector("#txt-so-x").value * 1;
  console.log("soX: ", soX);
  var ketQua = daoNguocSo(soX);

  console.log("ketQua: ", ketQua);
  document.querySelector("#ket-qua-5").innerHTML = `<div>
        <p> Kết quả: ${ketQua}</p>
        </div>`;
}

// BÀI 6
function btnSoNguyenDuongLonNhat() {
  var tong = 0;
  var ketQua = 0;
  for (var i = 0; i <= 100; i++) {
    tong += i;
    ketQua = i - 1;
    if (tong >= 100) {
      break;
    }
  }
  document.querySelector("#ket-qua-6").innerHTML = `<div>
        <p> Kết quả: y = ${ketQua}</p>
        </div>`;
}

// BÀI 7
function btnbangCuuChuong() {
  var soNhapVao = document.querySelector("#txt-number").value * 1;
  var ketQua = "";
  for (var i = 1; i <= 10; i++) {
    ketQua += `${soNhapVao} x ${i} = ${soNhapVao * i} </br>`;
  }
  document.querySelector("#ket-qua-7").innerHTML = `<div>
        <p>${ketQua}</p>
        </div>`;
}

// BÀI 8
function btnChiaBai() {
  var arrPlayer = [[], [], [], []];
  var arrCard = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  var ketQua = "";
  for (var i = 0; i < arrPlayer.length; i++) {
    for (var j = i; j < arrCard.length; j += 4) {
      ketQua += arrPlayer[i].push(arrCard[j]);
    }
    ketQua += "<br/>";
  }

  var hienThi = "";
  for (var i = 0; i < arrPlayer.length; i++) {
    var laBai = `<p>Player ${i + 1} = ${arrPlayer[i]}</p>`;
    hienThi += laBai;
  }

  document.querySelector("#ket-qua-8").innerHTML = `<div>
        <p>${hienThi}</p>
        </div>`;
}

// BÀI 9
function btnTinhSoGaCho() {
  var soGaCho = document.querySelector("#txt-ga-cho").value * 1;
  var soChan = document.querySelector("#txt-so-chan").value * 1;
  console.log({ soGaCho, soChan });
  var ga = 0;
  var cho = 0;
  for (var i = 0; i < soGaCho; i++) {
    for (var j = 0; j < soGaCho; j++) {
      if (i + j == soGaCho && i * 2 + j * 4 == soChan) {
        ga += i;
        cho += j;
      }
    }
  }
  document.querySelector("#ket-qua-9").innerHTML = `<div>
        <p>Số gà: ${ga} con, Số chó: ${cho} con</p>
        </div>`;
}

// BÀI 10
function tinhGoc(gio, phut) {
  var gocPhut = phut * 6;
  var gocGio = gio * 30 + phut * 0.5;
  var gocLech = Math.abs(gocGio - gocPhut);
  return Math.min(gocLech, 360 - gocLech);
}

function btnTinhGocLech() {
  var soGio = document.querySelector("#txt-so-gio").value * 1;
  var soPhut = document.querySelector("#txt-so-phut").value * 1;
  var ketQua = "";
  var gocLech = tinhGoc(soGio, soPhut);

  if (soGio >= 0 && soPhut >= 0) {
    ketQua = document.querySelector("#ket-qua-10").innerHTML = `<div>
    <p>Góc lệch: ${gocLech} độ</p>
    </div>`;
  } else {
    ketQua = document.querySelector("#ket-qua-10").innerHTML = `<div>
    <p>Góc lệch: Giờ và phút không có số âm</p>
    </div>`;
  }
}
